angle = 0.0
rad = 200

def setup():
    size(500,500)
    background(0)
    noFill()


def draw():
    global angle
    background(0)

    x = rad*cos(angle - PI/2)
    y = rad*sin(angle - PI/2)


    strokeWeight(2)
    stroke(255,0,255)
    axis()

    stroke(255)
    strokeWeight(1)
    cCircle()

    stroke(0,255,0)
    strokeWeight(5)
    pOnC(x,y)

    strokeWeight(2)
    stroke(255,0,0)
    cLines(x,y)

    angle += 0.01;


def cCircle():
    global rad
    ellipse(width/2, width/2, 2*rad, 2*rad)

def pOnC(x,y):
    strokeWeight(10)
    point(width/2+x,width/2+y)

def cLines(x,y):
    line(width/2, width/2, width/2+x,width/2+y)
    line(width/2+x,width/2+y, width/2+x, width/2)


def axis():
    line(0,width/2,width, width/2)
