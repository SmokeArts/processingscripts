add_library('peasycam')
points = []
r = 1
ang = 0

def setup():
    global cam
    global points
    size(1000,1000,P3D)
    temp = []
    x = width/2 + cos(ang)*(1+1/2*cos(ang/2))
    y = height/2 + sin(ang)*(1+1/2*cos(ang/2))
    z = 1/2*sin(ang/2)
    temp.append(x)
    temp.append(y)
    temp.append(z)
    points.append(temp)
    
    
    cam = PeasyCam(this, 100)
    cam.setMinimumDistance(50)
    cam.setMaximumDistance(500)
    background(0)
    
def draw():
    global ang
    background(0)
    fill(255)
    stroke(255)
    beginShape()
    for i in range(len(points)):
        vertex(points[i][0], points[i][1], points[i][2])
    temp=[]
    x = width/2 + cos(ang)*(1/2*cos(ang/2))
    y = height/2 + sin(ang)*(1/2*cos(ang/2))
    z = 1/2*sin(ang/2)
    temp.append(x)
    temp.append(y)
    temp.append(z)
    vertex(x,y,z)
    fill(255)
    stroke(255)
    endShape(CLOSE)
    ang += 0.01
