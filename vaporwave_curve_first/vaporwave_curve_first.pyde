xper = []
res = 25

def setup():
    global xper
    fullScreen()
    #size(500,500)
    cnoise(False)
    noLoop()
    
    
def draw():
    background(0)
    fill(255,100,255, 100)
    stroke(255,100,255,150)
    yres = 10
    for y in range((height/2)/yres):
        fill(y*15,(100+y*13),255-y*13, 88)
        beginShape(TRIANGLE_STRIP)
        for x in range(width/res+1):
            vertex(x*res, height-20-(0.5*(y*y))*xper[x]+((y*y)))
        endShape(CLOSE)
    
    
def keyPressed():
    noiseSeed(int(random(100)))
    cnoise(True)
    redraw()
    
def cnoise(remake):
    global xper
    dlim = 0
    ulim = 50
    xn = 0.0
    for x in range(width/res+1):
        if remake:
            xper[x] = map(noise(xn), 0, 1, dlim, ulim)
        else:
            xper.append(map(noise(xn), 0, 1, dlim, ulim))
        xn += 0.1
