a = 200
b = 150

def setup():
    size(600,600)
    noFill()
    stroke(255)
    
def draw():
    background(0)
    ellipse(width/2, height/2, a*2,b*2)
    lp()
    
            
def lp():
    focus = sqrt(a*a-b*b)
    line(mouseX, mouseY, width/2-focus, height/2)
    line(mouseX, mouseY, width/2+focus, height/2)
    fill(200,100,200)
    noStroke()
    ellipse(width/2-focus, height/2, 10, 10)
    ellipse(width/2+focus, height/2, 10, 10)
    fill(100,0,200)
    ellipse(mouseX, mouseY, 10, 10)
    noFill()
    stroke(255)
