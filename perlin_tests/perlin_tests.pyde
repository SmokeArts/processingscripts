def setup():
    size(500,500)
    background(88)
    noLoop()
    
def draw():
    background(88)
    noFill()
    stroke(255)
    for i in range(1, height/10):
        beginShape()
        for x in range(0, width/20):
            vertex(x*width/20, 20+(i*10)-50*noise((x)*0.5))
        endShape()
    
    
def keyPressed():
    redraw()
