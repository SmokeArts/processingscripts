x1 = 0
x2 = 0
pts = []
t = 0
pop = 1

def setup():
    global x1, x2
    background(0)
    #size(500,500)
    fullScreen()
    pts.append(x1)
    x2 = width/2
    
def draw():
    global x1, t, pop
    background(88)
    noStroke()
    fill(200,0,200)
    ellipse(x2, height/2, 40,40)
    x1 = lerp(x1, x2, 0.06)
    fill(0,200,0)
    ellipse(x1, height/2, 40, 40)
    if not t > 40 and t%2 is 0:
        strokeWeight(40-int((t)))
    beginShape()
    stroke(0,200,0, 255-4.5*t)
    for i in range(0, len(pts)):
        vertex(pts[i], height/2)
    vertex(x1, height/2)
    endShape()
    if t > 15 and t % 2 is 0:
        for i in range(0, int(pop)):
            if len(pts) > 0:
                pts.pop(0)
                pop += 0.3
    pts.append(x1)
    t+= 1
    
def mousePressed():
    global x2, t, pts, pop
    x2 = mouseX
    t = 0
    pop = 1
    for i in range(len(pts)-1, -1, -1):
        pts.pop(i)
    
