
        uniform vec2 resolution;
        uniform float time;
        void main() {
            vec2 uv = -1. + 2. * gl_FragCoord.xy / resolution.xy;
    gl_FragColor = clamp( abs( vec4(
     uv.y * time - floor(uv.y * time) ,
     radians( exp2( sin( sin( sin( sin( fract( tan( sin( uv.x * atan( abs( sqrt( sin( degrees( abs( sin( tan( atan( abs( uv.x * cos( time * 3.) - floor(uv.x * cos( time * 3.)) ) ) ) * time) ) ) * time) ) ) ) - floor(uv.x * atan( abs( sqrt( sin( degrees( abs( sin( tan( atan( abs( uv.x * cos( time * 3.) - floor(uv.x * cos( time * 3.)) ) ) ) * time) ) ) * time) ) ) )) + time) * time) ) + time) * time) + time) + time) ) ) ,
     uv.x * 10. - floor(uv.x * 10.) ,
     1. ) ),0.,1.);}