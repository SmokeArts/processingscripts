phi = 0.1#0.9#0.6180339887

def setup():
    size(800,500)
    #fullScreen()
    background(0)
    rectMode(CORNERS)
    
def draw():
    global phi
    background(0)
    stroke(255)
    fill(100)
    #rect(0,0,width*phi, height)
    #rect(width*phi,0, width, height*phi)
    fib(width, height, 0, 0)
    #phi -= 0.001
    
    
def fib(w, h, x, y):
    if w < 1:
        return 
    #fill(phi*255, 0, 0)
    rect(x, y+h, x+w*phi, y)
    #fill(0, 255*phi, 0)
    rect(x+w*phi, y, x+w, y+h*phi)
    #fill(0, 0, phi*255)
    rect(x+w, y+h*phi, x+w*(phi*phi-phi+1), y+h)
    #fill(255*phi, 255*phi, 255*phi)
    rect(x+w*(phi*phi-phi+1), y+h, x+w*phi, y+h*(phi*phi-phi+1))
    #line(x, y+h, x+w*phi, y)
    #line(x+w*phi, y, x+w, y+h*phi)
    #line(x+w, y+h*phi, x+w*(phi*phi-phi+1), y+h)
    #line(x+w*(phi*phi-phi+1), y+h, x+w*phi, y+h*(phi*phi-phi+1))
    fib(w*(phi*phi-2*phi+1), h*(phi*phi-2*phi+1), x+w*phi, y+h*phi)
