int spac = 15;
float inc = 0.005;
float xoff = 500.0;
float yoff = 500.0;
float zoff = 0.0;
float goal = 0.0;
PVector ms = new PVector(0,0);
PVector me = new PVector(0,0);

void setup(){
  //fullScreen();
  size(400,800);
  noiseDetail(8, 0.001);
  stroke(0);
  colorMode(RGB);
}


void draw(){
  inc = 0.03;
   xoff =lerp(xoff, inc*frameCount/8+goal, 0.5);//frameCount*inc;
   for(int x=0; x < width/spac; x++){
      yoff =0;//frameCount*inc/1;
      for(int y=0; y < height/spac; y++){
         float nois1 = noise(xoff, yoff, zoff);
            //float nois2 = noise(nois1*100);
            //float nois3 = noise(nois2*255);
            //color c = color(nois1*255, nois2*255, nois3*255);
            //String hs = int();
            //println(hs);
            //int hi = unhex(hs);
            //println(map(nois1, 0, 1, #FFFFFF, #000000));
            int clr = int(map(nois1, 0, 1, #FFFFFF, -999999));
            fill(clr, 20);
            //fill(nois1*255);
            //fill(c);
            //stroke(50);
            rect(x*spac, y*spac, spac, spac);
            //ellipse(x*spac+spac/2, y*spac+spac/2, spac, spac);
            yoff += inc-0.012;
      }
      xoff += inc;
   }
   zoff += 0.007;
}


void keyPressed(){
  background(200);
  noiseSeed(int(random(1000000)));
  redraw();
}

void mouseReleased(){
  me = new PVector(mouseX, millis(), mouseY);
  mouskliccd();
  direction();
}

void mousePressed(){
  ms = new PVector(mouseX, millis(), mouseY);
}

void mouskliccd(){
  if(ms.x == me.x && ms.z == me.z){
  background(200);
  noiseSeed(int(random(1000000)));
  redraw();
  }
}

void direction(){
  goal = -1*(ms.x - me.x)/(2*(ms.y-me.y));
}
