spacer = 10
pts = [[[PVector(0,0)] for j in range(500/spacer)] for i in range(500/spacer)]


def setup():
    size(500,500)
    background(0)
    for i in pts:
        print(i)
    cpts()
    
    
def draw():
    background(0)
    #dpts()
    for x in range(width/spacer):
        for y in range(height/spacer):
            d = dist(pts[x][y].x, pts[x][y].y, mouseX, mouseY)
            #ellipse(pts[x][y].x, pts[x][y].y, 200/(d+1), 200/(d+1))
            ellipse(pts[x][y].x + 300/((d+50)), pts[x][y].y + 300/((d+50)), 4,4)

    
def cpts():
    global pts
    for x in range(width/spacer):
        for y in range(height/spacer):
            pts[x][y] = PVector(x*spacer+spacer/2, y*spacer+spacer/2)
       
       
def dpts():
    noStroke()
    fill(255)
    for x in range(width/spacer):
        for y in range(width/spacer):
            ellipse(pts[x][y].x, pts[x][y].y, 2, 2)
