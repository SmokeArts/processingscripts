mx, my, siz = 0, 0, 1
ed = 100

def setup():
    background(0)
    size(500,500)
    
def draw():
    dline()
    
def dline():
    global siz
    siz = lerp(siz, ed, 0.2)
    line(mx - siz, my, mx + siz, my)


def mousePressed():
    global mx, my, siz
    siz = 1
    strokeWeight(random(10))
    stroke(255, random(255), random(255))
    mx = mouseX
    my = mouseY
    
def keyPressed():
    background(0)
