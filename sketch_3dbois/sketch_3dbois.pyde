xsp = 10
ysp = 10
per = 0.1
zpts = []

def setup():
    global zpts, per
    zpts = [[None for x in range(int(width/xsp)+100)] for y in range(int(height/ysp)+100)]
    background(0)
    size(500,500,P3D)
    noLoop()
    setNoise()
    
    


def draw():
    setNoise()
    background(0)
    translate(width/2, height/2)
    rotateX(PI/3)
    translate(-width/2, -height/2+100)
    for y in range(0, int(height/ysp)-1):
        noFill()
        stroke(255,100,255,30)
        beginShape(TRIANGLE_STRIP)
        for x in range(0, int(width/xsp)):
             vertex(x*xsp, y*ysp, zpts[x][y])
             vertex(x*xsp, (y+1)*ysp, zpts[x][y+1])
        endShape()   
    
def keyPressed():
    noiseSeed(int(random(100)))
    redraw()
    

def setNoise():
    global per, zpts
    yper = 0.0
    for y in range(0, int(height/ysp)):
        xper = 0.0
        for x in range(0, int(width/xsp)):
            zpts[x][y] = map(noise(xper,yper), 0, 1, -60, 60)
            xper += 0.1
        yper += 0.1
