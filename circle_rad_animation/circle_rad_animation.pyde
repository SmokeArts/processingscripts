siz = 10
dra = False
mx = None
my = None
pts = []
ed = 100

def setup():
    size(500,500)
    background(0)
    noStroke()
    
def draw():
    if dra:
        dcirc()
    
def mousePressed():
    global dra, mx, my, siz
    fill(255, random(255), random(255))
    siz = 10
    mx = mouseX
    my = mouseY
    dra = True
    
def keyPressed():
    reset()
    
def dcirc():
    global siz, pts
    #if mouseX == mx and mouseY == my:
    #    siz = lerp(siz+5, 0, 0.2)
    #else: 
    siz = lerp(siz, ed, 0.2)
    ellipse(mx, my, 2*siz, 2*siz)
    pts.append(PVector(mx, my))
    
    
def conn(xp, xy):
    for i in range(len(pts)):
        if dist(xp, xy, pts[i].x, pts[i].y) <= ed:
            stroke(255)
            line(pts[i].x, pts[i].y, xp, xy)
            noStroke()

def reset():
    global siz, dra
    background(0)
    siz = 10
    dra = False
