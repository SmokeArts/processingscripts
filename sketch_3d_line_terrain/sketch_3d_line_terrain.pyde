spacing = 60
zoff = 0
strr = 30
ptl = []
ptr = []

def setup():
    global ptl, ptr
    size(500, 1000, P3D)
    for y in range(height/spacing + 20):
        ptl.append(PVector(0,0,0))
        ptr.append(PVector(0,0,0))
    

def draw():
    global zoff, ptl, ptr
    noFill()
    translate(width/2, height/2)
    rotateX(PI/3)
    translate(-width/2, -height/2-1000)
    background(0)
    beginShape()
    for y in range(height/spacing+20):
        beginShape()
        z = None
        for x in range(width/30):
            z = map(noise(x*0.06,y*0.06+zoff), 0, 1, 0, 255)
            stroke(255-z, z, 255)
            if x == 0:
                ptl[y] = PVector(30*x, y*spacing, z)
            elif x == width/30 -1:
                ptr[y] = PVector(30*x, y*spacing, z)
            vertex(30*x, y*spacing, z)
        noFill()
        endShape()
        
        fill(255)
        beginShape()
        for i in range(len(ptl)):
            vertex(ptl[i].x, ptl[i].y, ptl[i].z)
        fill(255)
        endShape(CLOSE)
        
        beginShape()
        for i in range(len(ptr)):
            vertex(ptr[i].x, ptr[i].y, ptr[i].z)
        fill(255)
        endShape(CLOSE)
            
        zoff -= 0.0001
    
