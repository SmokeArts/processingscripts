cirx = 0
ciry = 0
rad = 0
circ = [None for x in range(628)]
for i in circ:
    i = None

def setup():
    size(500,500)
    background(0)
    

def draw():
    #background(0)
    global cirx, ciry, rad
    cirx = width/2
    ciry = height/2
    rad = 70
    dsirc(cirx, ciry, rad)
    
    
def dsirc(x1, y1, d):
    global circ
    noFill()
    stroke(200)
    beginShape(TRIANGLE_STRIP)
    for i in range(628):
        x = x1 + d * cos(i/100.0)
        y = y1 + d * sin(i/100.0)
        vertex(x, y)
        circ[int(x)][int(y)] = i
    endShape()
    

def mousePressed():
    pt = pnt()
    line(mouseX, mouseY, pt.x, pt.y)
     
     
     
#def pullpts():
    
     
def cir(xof, yof):
    for i in range(628):
        x = x1 + d * cos(i/100.0)
        y = y1 + d * sin(i/100.0)
        vertex(x, y)
        cir.append(PVector(x,y))
    
    
def pnt():
    global cirx, ciry, rad
    n = rad/(dist(mouseX,mouseY,cirx, ciry))
    px = cirx + n*(mouseX-cirx)
    py = ciry + n*(mouseY-ciry)
    return PVector(px, py)
