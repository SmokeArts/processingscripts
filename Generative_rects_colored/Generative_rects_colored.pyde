import random

def setup():
    colorMode(RGB)
    size(800,800)
    fill(200)
    noStroke()
    noLoop()

def draw():
    background(200)
    col = 10
    siz = (width-5)/col
    for i in range(0,col):
        pos = 0
        for n in range(25):
            heig = random.randint(25,100)
            fill(150)
            rect(i*siz+8, pos+8, siz-5, heig-5,4)
            fill(random.randint(113,216),random.randint(62,148),random.randint(126,232))
            rect(i*siz+5, pos+5, siz-5, heig-5,4)
            pos += heig
    
def keyPressed():
    redraw()
