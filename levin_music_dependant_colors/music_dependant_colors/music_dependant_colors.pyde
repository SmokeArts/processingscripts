add_library('minim')

def setup():
    minim = Minim(this)
    global song
    song = minim.loadFile ("Jammerlappen.mp3", 500)
    song.play()
    size (500,500)
    frameRate (15) #Wenn zu hoch -> viel zu flashy und seizure-inducing
    
def draw():
    stroke (255)
    for i in range((song.bufferSize() - 1)):
        None
       
    #Hintergrundfarbe in Abhängigkeit des Audios     
    r = noise(int(100*song.left.get(0))*0.01)
    g = noise(int(100*song.right.get(1))*0.01)
    b = noise(r+g)
    background(r*255, g*255, b*255)
    
    #Informationen zum Output drucken
    text(song.left.get(i)*50, 50, 50)
    text(song.left.get(i)*50-song.right.get(i)*50, 100, 50) #Differenz links u. rechs
    text(song.left.get(0)*50-song.right.get(250)*50, 150, 50)
    text(frameRate, 1, 15)
    
    #Welle malen
    for i in range((song.bufferSize() - 1)):
        line (i, 100+song.left.get(i)*50, i+1, 100+song.left.get(i+1)*50)
