circs = []
brad = 10.0

def setup():
    size(500,500)
    colorMode(HSB)
    noStroke()
    #fullScreen()
    noFill()
    
def draw():
    global brad
    background(88)
    if len(circs) == 0 or circs[len(circs)-1].done:
        if brad > 1.0:
            brad -= 0.5
        addc()
    for i in circs:
        #if i.done == True:
        i.display()
    print(len(circs))
    
def addc():
    x, y = random(width), random(height)
    while(x < brad or y < brad or width-x < brad or height-y < brad or incirc(x,y)):
        x, y = random(width), random(height)
    c = cir(brad, x, y)
    circs.append(c)
    c.update()
    
class cir:
    inc = 1.0
    def __init__(self, rad1, x1, y1):
        self.rad = rad1
        self.x = x1
        self.y = y1
        self.done = False
    
    def grow(self):
            self.rad += self.inc
            
        
    def display(self):
        self.update()
        fill(self.rad*2, 255, 255)
        circle(self.x, self.y, self.rad*2)
        
    def update(self):
        if self.edgeIntersect() is False and self.circIntersect() is False :
            self.grow()
        else:
            self.done = True
            println("Done")
            
        
    def edgeIntersect(self):
        if(width-self.x > self.rad+self.inc and self.x > self.rad+self.inc and 
               height-self.y > self.rad+self.inc and self.y > self.rad+self.inc ):
            return False
        
    def circIntersect(self):
        for i in circs:
            if i is not self:
                if dist(self.x, self.y, i.x, i.y) < (self.rad + i.rad + 1):
                    return True
        return False
    
    
def incirc(x,y):
    for c in circs:
        if dist(x,y,c.x,c.y) < c.rad + brad:
            return True
    return False

def reset():
    global circs
    circs = []
    background(0)
    loop()

def keyPressed():
    if key == " ":
        loop()
    if key == "a":
        noLoop()
    if key == "r":
        reset()
