 int mult = 50;
 int spac = 10;

void setup(){
    size(500,500);
    background(200);
    resetSketch();
    stroke(50);
    noLoop();
}
 
 
void draw(){
   resetSketch();
 
   
}

void keyPressed(){
   noiseSeed(int(random(99999)));
   redraw();
}

void resetSketch(){
   for(int i=0; i < width; i+=spac){
     for(int n=0; n < height; n+=spac){
       float map = map(noise(i,n), 0, 1, 0, 255);
       color c = color(255-int(map),0,int(map));
       fill(c);
       ellipse(spac/2*(i+noise(i)), spac/2*(n+noise(n)), mult*noise(i,n), mult*noise(i,n));
     } 
 }
}
