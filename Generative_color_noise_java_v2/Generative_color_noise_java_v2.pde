int spac = 10;
float inc = 0.0009;
float xoff = 0.0;
float yoff = 0.0;
float zoff = 0.0;

void setup(){
  //fullScreen();
  size(800,800);
  noiseDetail(8, 0.1);
  colorMode(RGB, 1, 1, 1);
}


void draw(){
  background(0);
   xoff = 0;//frameCount*inc/5;
   for(int x=0; x < width/spac; x++){
      yoff =0;// frameCount*inc/5;
      for(int y=0; y < height/spac; y++){
         float nois1 = noise(xoff, yoff, zoff);
            float nois2 = noise(nois1*100);
            float nois3 = noise(nois2*1000);
            color c = color(nois1, nois2, nois3);
            String hs = ""+int(map(nois1, 0, 1, #000000, #FFFFFF));
            //println(hs);
            int hi = unhex(hs);
            fill(c);
            //fill(nois1*255);
            //fill(c);
            rect(x*spac, y*spac, spac, spac);
            //ellipse(x*spac+spac/2, y*spac+spac/2, spac, spac);
            yoff += inc;
      }
      xoff += inc;
   }
   zoff += 0.0008;
}


void keyPressed(){
  noiseSeed(int(random(1000000)));
  redraw();
}

void mousePressed(){
  noiseSeed(int(random(1000000)));
  redraw();
}
