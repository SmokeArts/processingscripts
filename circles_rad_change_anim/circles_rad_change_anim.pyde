spacing = 100
rad = 50
i = 1.0
vec = None

def setup():
    size(600,600)
    noStroke()
    
def draw():
    global i
    background(0)
    if i >= 0:
        fill(100,200,100)
    elif i < 0:
        fill(200,100,200)
    for x in range(width/spacing):
        xc = spacing * (x + 0.5)
        for y in range(height/spacing):
            yc = spacing * (y + 0.5)
            ellipse(xc, yc, rad*2*i, rad*2*i)
    print(i)
    num2()
    
def num():
    global vec, i
    if i == 1.0:
        vec = -0.05  
    elif i == -1:
        vec = 0.05
    elif i == 0:
        if vec == -0.1:
            vec = 0.1
        elif vec == 0.1:
            vec = -0.1
    i = round(i + vec, 2)
    
def num2():
    global vec, i
    lp = 0.5
    rp = 4
    if round(i, rp) == 1.0:
        vec = -1
    elif round(i, rp) == -1.0:
        vec = 1
    if vec < 0:
        i = lerp(i, -1, lp)
    elif vec > 0:
        i = lerp(i, 1, lp)
