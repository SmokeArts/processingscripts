ang = 0.0
rad = 200
pts = []

def setup():
    background(25)
    size(800,800)
    #noLoop()
    
def draw():
    global ang
    background(25)
    noStroke()
    x = width/2  + rad * cos(ang)
    y = height/2 + rad * sin(ang)
    fill(255)
    ellipse(x,y,20,20)
    ang = lerp(ang, 2*PI, 0.05)
    stroke(255)
    noFill()
    strokeWeight(3)
    beginShape()
    for i in range(0, 100):
        nx = width/2  + rad * cos(ang-i*0.01)
        ny = width/2  + rad * sin(ang-i*0.01)
        nx = nx + 0.3*i*int(round(random(-1,1)))*noise(ang)
        ny = ny + 0.3*i*int(round(random(-1,1)))*noise(ang)        
        vertex(nx, ny)
    endShape()
    
def keyPressed():
    global ang
    ang = 0
    redraw()
