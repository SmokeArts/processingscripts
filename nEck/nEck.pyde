rad = 200.0
n = 3
i = 0

def setup():
    size(800,800)
    background(0)
    
def draw():
    global i, n
    if i % 20 is 0:
        background(0)
        nEck(n)
        n = n+1
    i += 1
    
def nEck(k):
    stroke(255)
    noFill()
    beginShape()
    oneN = PI*2/k
    for i in range(1,k+1):
        x = width / 2 + rad * cos(i*oneN)
        y = height / 2 + rad * sin(i*oneN)
        fill(255)
        ellipse(x,y,10,10)
        noFill()
        vertex(x,y)
    endShape(CLOSE)
