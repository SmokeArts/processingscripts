mx, my, siz = 0, 0, 1
rad = 50
ran = 0

def setup():
    size(500,500)
    background(0)
    
def draw():
    dline()
    
def mousePressed():
    global mx, my, siz, ran
    siz = 1
    strokeWeight(random(10))
    stroke(255,random(255), random(255))
    mx = mouseX
    my = mouseY
    ran = random(2*PI)
    
def dline():
    global siz, mx, my
    x1 = siz * cos(ran)
    y1 = siz * sin(ran)
    x2 = siz * cos(PI+ran)
    y2 = siz * sin(PI+ran)
    siz = lerp(siz, rad, 0.2)
    line(mx + x1, my + y1, mx + x2, my + y2)
    
def keyPressed():
    background(0)
