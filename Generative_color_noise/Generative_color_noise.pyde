spac = 10
inc = 0.0001
xoff = 0.0
yoff = 0.0
zoff = 0.0

def setup():
    #fullScreen()
    size(800,800)

    
def draw():
    background(0)
    global zoff, xoff, yoff
    xoff = frameCount*inc
    for x in range(width/spac):
        yoff = 0
        for y in range(height/spac):
            nois1 = noise(xoff, yoff, zoff)
            #nois2 = noise(nois1)
            #nois3 = noise(nois2)
            #c = color(nois1*255, nois2*255, nois3*255)
            fill("#"+hex(int(nois1*16777215)))
            #fill(nois1*255)
            #fill(c)
            rect(x*spac, y*spac, spac, spac)
            #ellipse(x*spac+spac/2, y*spac+spac/2, spac, spac)
            yoff += inc
        xoff += inc
    zoff += 0.00001
    
    
def keyPressed():
    print("redraw")
    noiseSeed(int(random(1000000)))
    redraw()
