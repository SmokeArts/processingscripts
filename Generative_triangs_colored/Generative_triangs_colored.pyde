rad = 0
n = 3
clrs = []

def setup():
    #fullScreen()
    size(800,800)
    background(188)
    addclr()
    noLoop()
    noStroke()

def draw():
    global n, rad
    background(188)
    rad = round(random(50,150))
    print(rad)
    dis = int(width/rad)
    for k in range(0,dis):
        const1 = rad+k*2*rad     #down
        const2 = 2*rad/4+k*2*rad     #up
        for i in range(0, dis):   #down
            x1 = (k%2)*rad+i*rad*2
            triang(x1, const1-k*rad/2+k*10, False)
        for i in range(0, dis):  #up
            x2 = ((k+1)%2)*rad+i*rad*2
            triang(x2, const2-k*rad/2+k*10, True)
    
def triang(x1,y1,bol):
    shax = 3
    shay = 7
    obj = []
    fill(120)
    beginShape()
    oneN = PI*2/3
    if bol is False:
        for i in range(1,4):
            x = x1 + rad * cos(i*oneN-PI/2)+shax
            y = y1 + rad * sin(i*oneN-PI/2)+shay
            obj.append(PVector(x-shax,y-shay))
            vertex(x,y)
    elif bol is True:
        for i in range(1,4):
            x = x1 + rad * cos(i*oneN+PI/2)+shax
            y = y1 + rad * sin(i*oneN+PI/2)+shay
            obj.append(PVector(x-shax,y-shay))
            vertex(x,y)
    endShape(CLOSE)
    fill(clrs[int(random(0,len(clrs)))])
    beginShape()
    for i in range(0,3):
        vertex(obj[i].x, obj[i].y)
    endShape(CLOSE)
    
def addclr():
    global clrs
    clrs.append(color(0xCC945EAA))
    clrs.append(color(0xCC5E95AA))
    clrs.append(color(0xCC81A54B))
    clrs.append(color(0xCCDBD271))
    clrs.append(color(0xCC46AFB2))
    clrs.append(color(0xCC842AE5))
    clrs.append(color(0xCC953547))
    clrs.append(color(0xCC70C64E))
    
def keyPressed():
    redraw()
