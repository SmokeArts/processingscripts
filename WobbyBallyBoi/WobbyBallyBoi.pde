float rad = 100;
float intensity = 4;
float time = 0;
void setup(){
  //background(0);
  size(500,500);
  //translate(width/2,height/2);
  //fill(255);
  //noStroke();
  //stroke(255);
  //ellipse(0,0,rad*2, rad*2);

}
void draw(){
  background(0);
  stroke(255);
  noFill();
  beginShape();
  for(float i = 0.0; i <= TWO_PI; i+=0.01){
    float x = cos(i);
    float y = sin(i);
    float off = intensity*noise(x, y, time);
    
    vertex(width/2+x*(rad*off),height/2+y*rad);
  }
  endShape(CLOSE);
  time += 0.01;
  //intensity = lerp(intensity, 0.9, 0);
}


void wobble(){


}


void keyPressed(){
intensity = lerp(intensity, 0.9, 4);
}
