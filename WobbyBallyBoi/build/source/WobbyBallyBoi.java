import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class WobbyBallyBoi extends PApplet {

float rad = 100;
float nois = 0.1f;


public void setup(){
  //background(0);
  
  //translate(width/2,height/2);
  //fill(255);
  //noStroke();
  //stroke(255);
  //ellipse(0,0,rad*2, rad*2);

}
public void draw(){
  background(0);
  translate(width/2,height/2);
  beginShape();
  for(float i = 0.0f; i <= TWO_PI; i+=0.01f){
    float radoff = rad+noise(nois)*10;
    float otoff = rad+noise(i)*10;
    float x = radoff*cos(i);
    float y = otoff*sin(i);
    vertex(x,y);
  }
  endShape(CLOSE);
  nois = nois+0.1f;
}
  public void settings() {  size(500,500); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "WobbyBallyBoi" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
