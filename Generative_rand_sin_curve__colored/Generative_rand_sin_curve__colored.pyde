clrs = []
parx = 0.0
rc = []

def setup():
    global rc
    size(500,500)
    addclr()
    background(88)
    for i in range(0,5):
        rc.append(int(random(0,len(clrs))))
    #noLoop()
    
    
def draw():
    loop()
    global parx
    background(88)
    randsin(parx, 650, 0.0, clrs[rc[0]])
    randsin(parx+75, 550, 0.25, clrs[rc[1]])
    randsin(parx+150, 450, 0.55, clrs[rc[2]])
    randsin(parx+225, 350, 0.85, clrs[rc[3]])
    randsin(parx+300, 250, 0.1, clrs[rc[4]])
    parx = parx-5
    
def keyPressed():
    global rc
    for i in range(0,4):
        rc[i] = (int(random(0,len(clrs))))
    parx = 0.0
    redraw()
    
def randsin(parx, yoff, rand, clr):
    parn = rand
    spacing = 200
    newx = PI/spacing
    beginShape()
    fill(clr)
    stroke(255)
    for x in range(width):
        y = sin((x-parx)*newx)
        nois = 400*noise(parn)
        ny = yoff+y*40
        vertex(x, ny-nois)
        parn += spacing/105000.0
    vertex(width, 0)
    vertex(0,0)
    endShape()
    

def addclr():
    global clrs
    clrs.append(color(0xCC945EAA))
    clrs.append(color(0xCC5E95AA))
    clrs.append(color(0xCC81A54B))
    clrs.append(color(0xCCDBD271))
    clrs.append(color(0xCC46AFB2))
    clrs.append(color(0xCC842AE5))
    clrs.append(color(0xCC953547))
    clrs.append(color(0xCC70C64E))
