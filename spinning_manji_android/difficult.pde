class nani{
  PVector sx, ex = new PVector(1,1);
float sm, em = 0;
boolean drag, clicked = false;


float rot = 0;
boolean moving = true;
float nvel, vel = 0;
PVector mpos = new PVector(width/2, height/3);
boolean simple = true;

void setup() {
  size(500,500);
  //fullScreen();
  colorMode(HSB, 100);
}

void draw() {
  background(100, 100);
  mpos = new PVector(width/2, height/4);
  ellipse(mouseX, mouseY, 20, 20);
  line(mpos.x, mpos.y, mouseX, mouseY);
  translate(mpos.x, mpos.y);
  rotate(vel);
  println(vel);
  translate(-mpos.x, -mpos.y);
  crManji(mpos.x, mpos.y, 50);
  //vel = lerp(vel,nvel,0.02);
}



void getvel(){
  float dels = dist(ex.x, ex.y,sx.x, sx.y);
  int delt = round(em - sm);
  float vel = dels/delt;
  //println("sm:\t" + sm);
  //println("em: \t" + em);
  //println("delt: \t" + delt);
  //println(vel);
  vel = map(vel, 0, 0.5, 0, 10*PI);
  //println(vel);
  if (ex.x > sx.x){
    vel = -vel;
  }
}
 
void mouseClicked(){
   float dir = 0;
   if(sx.x > ex.x){
      dir = 1;
   }else{
     dir = -1; 
   }
   vel = dir*PI;
   /*float distance = dist(sx.x, 0, ex.x, 0);
   float time = em-sm;
   float velocity = dir*distance/time;*/
   println(dir);
  }



void mousePressed(){
   moving = true;
   if(simple){
      sx = new PVector(mouseX, mouseY);
      sm = millis();
   }
   println("pressed");
}


void mouseReleased(){
  moving = false;
  if(simple){
     ex = new PVector(mouseX, mouseY);
     em = millis();
  }else{
    nvel = atan2(width/2-mouseX, height/2-mouseY);
  }
   println("released"); 
}


void mouseDragged(){
    //if(movingLeft
     //angle1 = vel;//-atan2(width/2-mouseX, height/2-mouseY);
     //angle2 = vel; 
     //nvel = 0;
     if(simple){
        rot = atan2(width/2-mouseX, height/2-mouseY);
     }else{
       nvel = atan2(width/2-mouseX, height/2-mouseY);
     }
 }





void crManji(float x,float y, int siz){
  int len = siz;
  PVector p1 = new PVector(x-len,y-len);
  stroke(0);
  strokeWeight(5);
  noFill();
  
  beginShape();
  vertex(p1.x, p1.y);
  vertex(p1.x+len, p1.y);
  vertex(p1.x+len, p1.y+2*len);
  vertex(p1.x+2*len, p1.y+2*len);
  endShape();
  
  beginShape();
  vertex(p1.x+2*len, p1.y);
  vertex(p1.x+2*len, p1.y+len);
  vertex(p1.x, p1.y+len);
  vertex(p1.x, p1.y+2*len);
  endShape();
}
}
