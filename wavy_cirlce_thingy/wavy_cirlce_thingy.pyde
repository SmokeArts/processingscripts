spacer = 20
thic = 5
rad = 100
move = False
ptsc = []
movx = 0

def setup():
    size(600,600)
    noStroke()
    fill(200)
    #reset()
    
def draw():
    global ptsc
    background(0)
    if move == False:
        opt2()
    elif move == True:
        for i in range(len(ptsc)):
            fill(100,255,100)
            ellipse(ptsc[i].x, ptsc[i].y, 7,7)
            nx = ptsc[i].x + ptsc[i].z*ptsc[i].z
            ny = ptsc[i].y + ptsc[i].z*ptsc[i].z
            if i == 1:
                print(nx)
            ptsc[i] = PVector(lerp(ptsc[i].x, nx, 0.1),lerp(ptsc[i].y, ny, 0.1), ptsc[i].z)
                 
def mousePressed():
    global move
    move = True
    redraw()


def keyPressed():
    global move
    move = False
        
    
    
def opt2():
    global ptsc
    ptsc = []
    stroke(100)
    beginShape()
    noStroke()
    count = 0
    for x in range(width/spacer):
        for y in range(height/spacer):
            dis = dist(spacer*(x+0.5), spacer*(y+0.5), mouseX, mouseY)
            c = 10*log(dis+1)/log(rad+1)
            if x == 1 and y == 1:
                #print(c)
                g = 1
            if dis >rad:
                fill(255)
                ellipse(spacer*(x+0.5), spacer*(y+0.5), thic, thic)
                #vertex(spacer*(x+0.5), spacer*(y+0.5))
            elif dis <= rad:    
                fill(100,255,100)
                xc = spacer*(x+0.5)-c*c
                yc = spacer*(y+0.5)-c*c
                ellipse(xc , yc , c, c)
                noFill()
                vertex(xc, yc)
                ptsc.append(PVector(xc, yc, c))
    noFill()
    stroke(255, 255, 255, 100)
    endShape()
