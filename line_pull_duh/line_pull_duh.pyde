pts = [PVector(0, 0) for i in range(460)]


def setup():
    global pts
    background(0)
    size(500,500)
    for i in range(0, width-40):
        pts[i] = PVector(i, height/2)
    
def draw():
    #background(0)
    noFill()
    stroke(255)
    beginShape()
    for i in range(len(pts)):
        vertex(pts[i].x, pts[i].y)
    endShape()
    
def mousePressed():
    global pts
    ran = int(random(8))*2
    fill(255)
    for i in range(6):
        dig = mouseX-3+i
        p = getVec(dig)
        pts[dig] = PVector(dig - 1/2*p.x,dig - 1/2*p.y)
        #ellipse(dig + 1/2*p.x,dig + 1/2*p.y, 20, 20)
        
def getVec(xc):
    ar = PVector(pts[xc].x, pts[xc].y)
    return ar.sub(PVector(mouseX, mouseY))
