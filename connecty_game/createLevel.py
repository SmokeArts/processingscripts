import config as c
spac = c.spac
cols = c.cols
rows = c.rows
gend = None
np = None
ts = c.thingys
points = [[-2 for y in range(rows)] for x in range(cols)]
vecs = c.vecs

def getGrid():
    return points

def cLevel():
    global ts
    getend()
    while ts >= 1:
        getPoint()
        ts -= 1
    
     
def reset():
    global gend, np, points, ts
    points = [[-2 for y in range(rows)] for x in range(cols)]
    gend = None
    np = None
    ts = 20
            
                                                        
def cGrid():
    for x in range(cols):
        for y in range(rows):
            a = points[x][y]
            if a == -2:
                fill(0)
            elif a == -1:
                fill(88)
            elif a == 1:
                fill(0,255,0)
            elif a == c.thingys:
                fill(255,0,0)
            else:
                fill(0,255,0)
            square(x * spac, y * spac, spac-1)
            fill(255,255,255)
            
            textSize(20)
            text(points[x][y], x*spac+0.5*spac, y*spac+0.5*spac)
    
        
def level():
    fill(88)
    for x in range(cols):
        for y in range(rows):
            a = points[x][y]
            if a == -2:
                fill(0)
            elif a == -1:
                fill(88)
            elif a == 1:
                fill(0,0,255)
            elif a == c.thingys:
                fill(255,0,0)
            else:
                fill(88)
            square(x * spac, y * spac, spac-1)
    
def getend():
    global gend, points, ts
    gend = PVector(int(random(cols)), int(random(rows)))
    points[int(gend.x)][int(gend.y)] = ts
    ts -= 1
    
    
def getPoint():
    global gend, np, points, error
    if np is None:
        np = gend
    saf = PVector(np.x, np.y)
    
    vcs = list(vecs)
    np.add(vcs.pop(int(random(len(vcs)))))
    while not aviable(np):
        if len(vcs) == 0:
            print("ERROR WITH VCS")
            print(saf)
            error = True
            return 
        np = PVector(saf.x, saf.y)
        np.add(vcs.pop(int(random(len(vcs)))))
    points[int(np.x)][int(np.y)] = ts
    
    
def aviable(p):
    if p.x <= cols-1 and p.y <= rows-1 and p.x >= 0 and p.y >= 0:
        if points[int(p.x)][int(p.y)] == -2:
            return True
    return False
    
