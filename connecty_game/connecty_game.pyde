import time
import createLevel as cl
import selecting as sel
import config as c
spac = c.spac
cols = c.cols
rows = c.rows
#clevel = False

#   Integrate selecting into the game: kinda combine points and add new number like -1 ? for "selected square"
#   Code some thing to force player tt start from first point 
#   Check if all things turned to new selected number thingy and then level complete

def setup():
    size(spac*cols,rows*spac)
    cl.cLevel()
    #println(cl.getGrid())
    sel.reset(cl.getGrid())
    #println(sel.points)
    println("setup cmplete")
        
def draw():
    background(0)
    #cl.cGrid()
    a = sel.dpoints()
    if a == 1:
        time.sleep(2)
        reset()
        
      
def reset():
    cl.reset()
    cl.cLevel()
    sel.reset(cl.getGrid())
    redraw()      

def mouseDragged():
    sel.setDragged(True)
    sel.checkPts()
    
    
def mousePressed():
    sel.checkPts()
    
    
def mouseReleased():
    sel.setDragged(False)


def keyPressed():
    global clevel
    if key == "r" or key == "R":
        reset()
        println("reset complete")
    elif key == "c":
        cl.cLevel()
        clevel = True
        print("created level")
