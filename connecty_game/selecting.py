import config as c
import time
spac = c.spac
cols = c.cols
rows = c.rows
vecs = c.vecs
points = [[-1 for y in range(rows)] for x in range(cols)]
dragged = False
lastnum = 1
justActivated = PVector(0,0)
done = False

def setGrid(inp):
    global points
    points = inp
    reformatGrid()


def reset(grid):
    global lastnum, justActivated, dragged, done
    lastnum = 1
    justActivated = PVector(0,0)
    dragged = False
    setGrid(grid)
    done = False

def setDragged(b):
    global dragged
    dragged = b
    

def reformatGrid():
    global points, justActivated
    for x in range(cols):
        for y in range(rows):
            a = points[x][y]
            if a == 1:
                justActivated = PVector(x,y)
                points[x][y] = 1
            if a > 1 and a < c.thingys:
                points[x][y] = -1

    
def dpoints():
    if done is True:
        print("Done")
        for x in range(cols):
            for y in range(rows):
                a = points[x][y]
                if a == -2:
                    fill(0)
                else:# points[x][y] > 0:
                    fill(0,255,0)
                square(x*spac, y*spac, spac-1)
                
        return 1

    else:
        for x in range(cols):
            for y in range(rows):
                a = points[x][y]
                if a == -2:
                    fill(0)
                elif a == -1:
                    fill(88)
                elif a == 1:
                    fill(0,0,255)
                elif a == c.thingys:
                    if done is True:
                        fill(0,255,0)
                    else:
                        fill(255,0,0)
                else:
                    fill(0,200,0)
                    
                square(x*spac, y*spac, spac-1)
                #fill(255,255,255)
                
                #textSize(20)
                #text(points[x][y], x*spac+0.5*spac, y*spac+0.5*spac)
        
        
    
    
def checkPts():
    global points, justActivated, addedPts, lastnum, done
    x = int(round(mouseX/spac))
    y = int(round(mouseY/spac))
    if points[x][y] == -1 and checkSquare(x*spac,y*spac) is True and squareWasntLastSquare(x,y) and adjacentSquare(x,y):
        lastnum += 1
        points[x][y] = lastnum
        justActivated = PVector(x,y)
    elif points[x][y] > 0 and points[x][y] < c.thingys and dragged is False:
        deletePoints(points[x][y])
    elif points[x][y] == c.thingys and everythingelse() is True:
        done = True
    
    
def everythingelse():
    count = 0
    for x in range(cols):
        for y in range(rows):
            if points[x][y] >= 1:
                count += 1
    if count == c.thingys:
        return True
    else:
        return False


def deletePoints(num):
    global points, justActivated, lastnum
    for x in range(cols):
        for y in range(rows):
            if points[x][y] == 1:
                justActivated = PVector(x,y)
            elif points[x][y] >= num and points[x][y] != c.thingys:
                println(str(x)+str(y))
                println(points[x][y])
                points[x][y] = -1
                lastnum -= 1
            elif points[x][y] == num-1:
                justActivated = PVector(x,y)
    print(justActivated)

def keyPressed():
    if key == "p":
        print(points)


def adjacentSquare(x,y):
    for i in vecs:
        if PVector(justActivated.x, justActivated.y).add(i) == PVector(x,y):
            return True
    return False
    

def squareWasntLastSquare(x,y):
    if justActivated != PVector(x,y):
        return True
    else:
        return False

def checkSquare(x,y):
    if (mouseX > x and mouseX < x+spac and mouseY > y and mouseY < y+spac):
        return True
    else:
        return False
