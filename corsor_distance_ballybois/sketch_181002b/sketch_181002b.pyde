def setup():
    size(500,500);
    colorMode(RGB)
    stroke(255)
    #noStroke()

def draw():
    background(0)
    maxDistance = dist(width/2, width/2, 0, 0)
    for x in range(0, width, 25):
        for y in range (0, width, 25):
            d = dist(mouseX, mouseY, x, y)/maxDistance
            fill(d*190, 0, 255)
            ellipse(x, y, 50*d, 50*d)
