spac = 10
inc = 0.0007
zoff = 0.0

def setup():
    fullScreen()
    #size(500,500)
    #noLoop()
    #frameRate(5)
    noStroke()
    noFill()
    background(60)
    
def draw():
    global zoff
    xoff = frameCount * inc/2
    background(0)
    xoff = 0.0
    for x in range(width/spac):
        yoff = 0.0
        for y in range(height/spac):
            c = noise(xoff, yoff, zoff)
            c1 = noise(c*100)
            c2 = noise(c*2000)
            fill(c*255, c1*255, c2*255)
            square(x*spac, y*spac, spac-1)
            yoff += inc
        xoff += inc
        
    zoff += 0.0005
        
    
    
def keyPressed():
    noiseSeed(long(random(99999)))
    redraw()
