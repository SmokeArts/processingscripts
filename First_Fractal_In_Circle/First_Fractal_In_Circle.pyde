wid = 800.0

def setup():
    background(0)
    size(1000,1000)
    noFill()
    stroke(255)
    #ellipse(0,0,400,400)
    
def draw():
    global wid
    background(0)
    noFill()
    stroke(255)
    ellipse(width/2,width/2,wid,wid)
    rec(2,wid/2)
    wid += 20
    
def recdraw(x, diam):
    #translate(width/2,height/2)
    if x < 0:
        ellipse(x,0,diam,diam)
    else:
        ellipse(-x,0,diam,diam)
    ellipse(x,0,diam,diam)
    if diam >1:
        #ellipse(x/2+x,0,diam/2, diam/2)
        recdraw(wid-x/2, diam/2)
        #translate(width/2,0)
        recdraw(x/2, diam/2)
        
        
def rec(n,rad):
    stroke(255)
    noFill()
    for i in range(1,n*2):
        if i % 2 is 0:
            continue
        num = wid/(2*n)
        x = (i*num)
        ellipse(width/2-wid/2+(i*num),height/2,rad,rad)
    if n < 500:
        rec(n*2, rad/2)
