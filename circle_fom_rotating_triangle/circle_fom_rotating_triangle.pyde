rad = 150
ang = 0
angA = 0.0
angB = 0.0
angC = 0.0
A = PVector(0,0)
B = PVector(0,0)
C = PVector(0,0)
R = PVector(0,0)
hypot = ""

def setup():
    global ang
    global R
    size(800,800)
    background(0)
    noFill()
    stroke(255,50)
    translate(width/2, height/2)
    createTriangle(0,0,100,-100,0,200)
    fill(255)
    randPoint()
    ellipse(R.x,R.y,5,5)
    ang += 0.1



def draw():
    global ang, A, R
    #translate(width/2, height/2)
    ellipse(R.x,R.y,5,5)
    standart()
    #getAng()
    #rotOne(A,angA)
    
def rotAll():
    global A,B,C
    rotOne(A)
    rotOne(B)
    rotOne(C)
    beginShape()
    vertex(A.x,A.y)
    vertex(B.x,B.y)
    vertex(C.x,C.y)
    endShape(CLOSE)
    
def rotOne(P, ang):
    d = dist(P.x,P.y,R.x,R.y)
    x = d*sin(ang)
    y = d*cos(ang)
    P.set(x,y)
    translate(width/2, height/2)
    noFill()
    stroke(255,50)
    ellipse(x,y,3,3)
    ang += 0.01
    
def getAng():
    global angA, angB, angC
    da = dist(A.x,A.y,R.x,R.y)
    angA = acos(da/abs(A.x-R.x))
    
    db = dist(A.x,B.y,R.x,R.y)
    angB = acos(db/abs(B.x-R.x))
    
    dc = dist(C.x,C.y,R.x,R.y)
    angC = acos(dc/abs(C.x-R.x))
    
def getHypot():
    global hypot
    ab = dist(A.x,A.y,B.x,B.y)
    ac = dist(A.x,A.y,C.x,C.y)
    bc = dist(B.x,B.y,C.x,C.y)
    if ab > ac and ab > bc:
        hypot = "ab"
    elif ac > ab and ac > bc:
        hypot = "ac"
    elif bc > ab and bc > ac:
        hypot = "bc"


def randPoint():
    translate(width/2, height/2)
    global R
    a = PVector(A.x, A.y)
    r = random(0,1)
    s = random(r,1)
    if hypot == "ab":
        a = PVector(A.x, A.y).add(PVector(C.x, C.y).sub(PVector(A.x, A.y)).mult(r))
        b = PVector(B.x, B.y).sub(PVector(B.x, B.y).sub(PVector(C.x, C.y)).mult(s))
        r = b.sub(a)
        R.set(r.x, r.y)
        ellipse(r.x,r.y,5,5)
        return r
    elif hypot == "ac":
        a = PVector(A.x, A.y).add(PVector(B.x, B.y).sub(PVector(A.x, A.y)).mult(s))
        b = PVector(B.x,B.y).add(PVector(C.x, C.y).sub(PVector(B.x, B.y)).mult(r))
        r = a.add(b).sub(PVector(B.x,B.y))
        R.set(r.x, r.y)
        ellipse(r.x,r.y,5,5)
        return r
    elif hypot == "bc":
        a = PVector(B.x, B.y).sub(PVector(B.x, B.y).sub(PVector(A.x, A.y)).mult(s))
        b = PVector(A.x,A.y).add(PVector(C.x, C.y).sub(PVector(A.x, A.y)).mult(r))
        r = a.add(b)
        R.set(r.x, r.y)
        ellipse(r.x,r.y,5,5)
        return r


def createTriangle(x1,y1,x2,y2,x3,y3):
    global A,B,C
    A = PVector(x1,y1)
    B = PVector(x2,y2)
    C = PVector(x3,y3)
    noFill()
    beginShape()
    vertex(A.x, A.y)
    vertex(B.x,B.y)
    vertex(C.x,C.y)
    endShape(CLOSE)
    getHypot()


def standart():
    global ang
    if ang is 2*PI:
        noLoop()
    stroke(255,50)
    noFill()
    beginShape()
    a = 4
    for i in range(1,a):
        arg = ang-PI/2
        offset = i*2*PI/3
        x = width/2+rad*cos(arg+offset)
        y = height/2+rad*sin(arg+offset)
        vertex(x,y)
    endShape(CLOSE)
    ang += 0.01
