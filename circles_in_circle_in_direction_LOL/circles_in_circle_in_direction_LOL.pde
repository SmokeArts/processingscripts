void setup(){
   size(500,500);
   background(200);
   noLoop();
   noFill();
   stroke(100);
}

void draw(){
   dircirc(random(width), random(height), 100);
}

void keyPressed(){
  println(int(random(2))*2-1);
   redraw();
}


void multcrc(float x, float y, int s){
   ellipse(x,y,s,s);
   for(int i=2; i < s/10; i++){
      ellipse(x+10*noise(i), y+10*noise(i),s/i, s/i);
   }
   
}

void dircirc(float x, float y, int s){
  float slp = random(-1, 1);
  ellipse(x,y,s,s);
   for(int i=2; i < 50; i++){
      ellipse(x+i, y, i*s/(20*i), i*s/(20*i));
   }
   
}
