dis = 50

def setup():
    size(500,500)
    background(0)
    b = sqrt(10000-2500)
    beginShape()
    noFill()
    stroke(255)
    vertex(width/2 - 50, height/2 - 50)
    vertex(width/2 + 50, height/2 - 50)
    vertex(width/2 + 50 + b, height/2)
    vertex(width/2 + 50, height/2 + 50)
    vertex(width/2 - 50, height/2 + 50)
    vertex(width/2 - 50 - b, height/2)
    endShape(CLOSE)
    
    
def draw():
     i = 0
