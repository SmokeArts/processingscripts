p1 = []
p2 = PVector(0,0)
t = 0
newx = 0
newy = 0
nois = 0.0


def setup():
    global p1, p2, newx
    fullScreen()
    #background(0)
    size(500,500)
    p1.append(PVector(0, height/2))
    newx = p1[0].x
    newy = p1[0].y
    #p2.x = width/2
    #p2.y = height/2
    noStroke()
    fill(255,100,255)
    ellipse(p1[0].x, p1[0].y, 10, 10)
    fill(100,255,100)
    ellipse(p2.x, p2.y, 10, 10)
    
def draw():
    global p1, p2, t, newx, newy, nois
    background(88)
    noStroke()
    fill(100,255,100)
    ellipse(p2.x, p2.y, 10, 10)
    fill(255,100,255)
    newx = lerp(newx, p2.x, 0.07)
    newy = lerp(newy, p2.y, 0.07)
    newp = PVector(newx, newy)
    print(len(p1))
    if len(p1) >50:
        p1.pop(0)
    p1.append(newp)
    ellipse(newp.x, newp.y, 10, 10)
    noFill()
    stroke(255)
    strokeWeight(3)
    beginShape()
    #print(int(round(random(-1,1))))
    #if t <= 70:
        #p1[t-1].x = p1[t-1].x+10*int(round(random(-1,1)))*noise(nois)
        #p1[t-1].y = p1[t-1].y+10*int(round(random(-1,1)))*noise(nois)
    fac = dist(newx, newy, p2.x, p2.y)/17.0
    for i in range(0, len(p1)-1):
        p1[i].x = p1[i].x+fac*int(round(random(-1,1)))*noise(nois)
        p1[i].y = p1[i].y+fac*int(round(random(-1,1)))*noise(nois)
        vertex(p1[i].x, p1[i].y)
    vertex(newx, newy)
    endShape()
    nois += 0.1
    t += 1
    
    
def mousePressed():
    global p1, p2, t
    p2.x = mouseX
    p2.y = mouseY
    for i in range(0, len(p1)-1):
        p1.pop(0)
    t = 0
    
