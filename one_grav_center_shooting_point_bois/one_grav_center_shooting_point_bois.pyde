startPoint = None
sVector = None
newsPVector = None
writevec = False
fly = False
multiplier = 0
n = 0.01

def setup():
    size(600,600)
    
def draw():
    background(0)
    dott()
    mousedot()
    vector()
    if fly is True:
        mpoint()


def mousePressed():
    global startPoint, sVector, writevec, fly
    if mouseButton == LEFT and writevec and not fly:
        fly = True
        print("set vector")
    if mouseButton == LEFT and not writevec:
        startPoint = PVector(mouseX, mouseY)
        sVector = PVector(mouseX, mouseY).sub(PVector(startPoint.x, startPoint.y))
        writevec = True
        print("set start point")

def keyPressed():
    if key == "r" or key == "R":
        print("Resetting")
        background(0)
        reset()
        redraw()

def dott():
    fill(255,0,0)
    noStroke()
    ellipse(width/2, height/2, 10, 10)

def mousedot():
    fill(0,255,0)
    noStroke()
    if startPoint is None:
        ellipse(mouseX, mouseY, 7, 7)
    else:
        ellipse(startPoint.x, startPoint.y, 7, 7)

def vector():
    global sPVector
    if writevec and not fly:
        sVector = PVector(mouseX, mouseY).sub(PVector(startPoint.x, startPoint.y))
        sp = PVector(startPoint.x - sVector.x, startPoint.y - sVector.y)
        stroke(100,200,100)
        noFill()
        line(startPoint.x, startPoint.y, sp.x, sp.y)


def mpoint():
    global multiplier, n
    sp = PVector(startPoint.x - sVector.x, startPoint.y - sVector.y).mult(n)
    fill(0,255,0)
    noStroke()
    n += 0.01
    #multiplier = lerp(multiplier, 2, 0.01)
    if startPoint.x < sp.x and startPoint.y < sp.y:
        print("s < sp")
        ellipse(startPoint.x + sp.x, startPoint.y + sp.y, 7, 7)
    elif startPoint.x < sp.x and startPoint.y > sp.y:
        na = "ni"
    if startPoint.x > sp.x and startPoint.y < sp.y:
        na = "ni"
    elif startPoint.x > sp.x and startPoint.y > sp.y:
        na = "ni"


def reset():
    global startPoint, sVector, newsPVector, writevec, fly, multiplier, n
    startPoint = None
    sVector = None
    newsPVector = None
    writevec = False
    fly = False
    multiplier = 0
    n = 0.01
