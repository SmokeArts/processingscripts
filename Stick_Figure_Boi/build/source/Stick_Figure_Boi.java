import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Stick_Figure_Boi extends PApplet {

Boi myBoi;
float ang = 0;
float a2 = 0.0f;
public void setup(){

background(0);
ellipse(200,200,10,10);
myBoi = new Boi(200,200,100);
myBoi.setPoints(200,200,200,360);

}

public void draw(){
  background(0);
  myBoi.setPoints(200,200,200,360);
myBoi.upperbody(88,100,a2);
myBoi.hips(100,ang);
ang += 1;
a2 += 10;

}
class Boi{
  float x;
  float y;
  PVector one = new PVector(width/2, height/2);
  PVector two = new PVector(width/2, height/2);
  int scale = 100;
  public Boi(float x1, float y1, int s){
    this.x = x1;
    this.y = y1;
    this.scale = s;
  }
  
  public void drawBoi(){
  
 }
 public void setPoints(float x1, float y1, float x2, float y2){
 one.set(x1,y1);
 two.set(x2,y2);
 }

 
 
 public void upperbody(float head, float al, float ang){
   noFill();
   stroke(255);
   print(one.x);
   print(one.y);
  ellipse(one.x, one.y - head / 2, head, head);
  x = one.x+al*cos(ang);
  y = one.y+al*sin(ang);
  float xs = one.x-al*cos(ang);
  print(x);
print(y);
  line(one.x, one.y, x, y);
  line(one.x, one.y, xs, y);
  line(one.x, one.y, two.x, two.y);
 }  
 public void hips(float ll, float ang){
 float x1 = two.x+ll*cos(ang);
 float y1 = two.y+ll*sin(ang);
 float x2 = two.x-ll*cos(ang);
 line(two.x, two.y, x1, y1);
 line(two.x, two.y, x2, y1);
 }
 
 
 public void test(){
   ellipse(300,300,50,50);
 }
}
  public void settings() { 
size(500,500); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Stick_Figure_Boi" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
