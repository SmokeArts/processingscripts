class blob:
    def __init__(self, srad, xpos, ypos):
        self.rad = srad
        self.pos = PVector(xpos, ypos)
        vel = PVector.random2D();
        self.vel = vel.mult(random(2,5))
        
    def show(self):
        noFill()
        stroke(255)
        circle(self.pos.x, self.pos.y, self.rad*2)
        
    def update(self):
        self.pos.add(self.vel)
        
        if self.pos.x > width-self.rad or self.pos.x < self.rad:
            self.vel.x = self.vel.x*-1
            
        if self.pos.y > width-self.rad or self.pos.y < self.rad:
            self.vel.y = self.vel.y*-1
            
