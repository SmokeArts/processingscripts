p1 = PVector()
p2 = PVector()
i = 69.0
ll = []
n = 0.0

def setup():
    size(500,500)
    background(0)
    createArr(100, width-100)
    
def createArr(x1, x2):
    global p1, p2, i, ll
    p1.set(x1, height/2)
    p2.set(x2, height/2)
    interval = (x2-x1)/i
    noFill()
    stroke(255)
    beginShape()
    for k in range(1,int(i)):
        vertex(x1 + k*interval, height/2)
        ll.append(PVector(x1 + k*interval, height/2))
    endShape()
    
def draw():
    background(0)
    bgStroke()
    noiseStroke(100)
    
def noiseStroke(ns):
    global n, ll
    noFill()
    stroke(255)
    beginShape()
    vertex(100, height/2)
    asa = 0
    for k in ll:    
        vertex(k.x, k.y - ns/2 + ns*noise(n))
        n += 0.5
    vertex(width-100, height/2)
    endShape()  

def bgStroke():
    stroke(80)
    noFill()
    line(p1.x, p1.y, p2.x, p2.y)
