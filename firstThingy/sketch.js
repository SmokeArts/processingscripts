var n = 0;
var c = 4;

function setup(){
  createCanvas(400,400);
  angleMode(DEGREES);
  colorMode(HSB);
  background(0);
}

function draw(){
  var a = n * 139.3;
  var r = c * sqrt(n);
  var x = r * cos(a) + width/2;
  var y = r * sin(a) + height/2;
  fill((a-r)%255,100,100)
  noStroke();
  ellipse(x,y, log(n), log(n));
  n++;
  if (n>2000) {
    noLoop();
  }
}
