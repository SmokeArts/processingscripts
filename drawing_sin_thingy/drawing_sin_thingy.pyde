ang = 0
amp = 100
pts = []
ranpts = []
amount = 1
n = 0

def setup():
    global pts
    size(500,500)
    for i in range(width):
        p = PVector(i, width/2 + amp*sin(i*PI/200 + ang))
        vertex(p.x, p.y)
        #pts.append(p)
        
    rpts(False)
    
def draw():
    global ang, pts, ranpts, n
    background(0)
    beginShape()
    stroke(255)
    noFill()
    for i in range(n):
        p = PVector(i, width/2 + amp*sin(i*PI/200 + ang))
        pts.append(p)
        vertex(p.x, p.y)
        
    
    for i in range(len(ranpts)):
        r = int(ranpts[i].z)
        stroke(0,255,255)
        #ellipse(ranpts[i].x, ranpts[i].y, 10, 10)
        if r == 0:
            r = 500
        if r == 500:
            r = 0
        fill(255)
        ellipse(ranpts[i].x, ranpts[i].y, 10, 10)
        if len(pts) > 2:
            line(ranpts[i].x, ranpts[i].y, pts[len(pts)-1].x, pts[len(pts)-1].y)
        ranpts[i].z = r-1
        
        
    stroke(255) 
    noFill()
    endShape()
    n += 1
    if n > width:
        n = 0
    #ang += 0.05
    
def keyPressed():
    global ang
    if key == " ":
        noLoop()
        
    if key == "r":
        ang = 0
        loop()
        
    if key == "n" or key == "N":
        rpts(True)
        
        
def rpts(reset):
    global ranpts
    if reset:
        ranpts = []
    for i in range(amount):
        ranpts.append(PVector(random(500), random(500), int(random(len(pts)))))
