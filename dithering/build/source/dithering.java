import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class dithering extends PApplet {

PImage pic;

public void setup(){
  pic = loadImage("img.jpg");
  
  image(pic, 0, 0);
  fill(0);
  noStroke();
  rect(500,0,50,500);

  makeRandDisturb();
}

public void draw(){
  //pic.loadPixels();
  /*for(int x=0; x < pic.height; x++){
     for(int y=0; y < pic.width; y++){
        int index = x + y * pic.width;
     }

  }
  pic.updatePixels();
  image(pic, 550, 0);
*/
}

public void makeRandDisturb(){
  pic.loadPixels();
for(int i=0; i < 100; i++){
           int randX = round(random(100,400));
           int randY = round(random(100,400));
           int index = randX + randY * pic.width;
           int pix = pic.pixels[index];

           float r = red(pix);
           float g = green(pix);
           float b = blue(pix);

           float r1 = round(r/255)*100;
           float g1 = round(g/255)*100;
           float b1 = round(b/255)*100;

           int nc = color(r1,g1,b1);
           pic.pixels[index] = nc;

           for(int j=0; j < 300; j++){
              //int randXoff = round(random(-1,1));
              //int randYoff = round(random(-1,1));
                 for(int n=0; n < 30; n++){
                    pic.pixels[index+j] = nc;
                 }



           }
      }
      pic.updatePixels();
      image(pic, 550, 0);
}
  public void settings() {  size(1050, 500); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "dithering" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
