import math
scale = 100
diam = 80
rowss = 800/scale -1
cols = 800/scale -1
angle = 0
shapes = []

def setup():
    size(800,800);
    setArray()
    noLoop();

def keyPressed():
    loop()

def draw():
    global angle
    global shapes
    background(0)
    circles()
    #noLoop()
    doall()
    angle += 0.05
    #else:
        #angle = 0.0
        #for i in range(rowss):
            #for n in range(cols):
                #shapes[i][n] = []

def doall():
    for i in range(rowss):
        arg1 = (i+1)*angle-PI/2
        x1 = diam/2*cos(arg1)
        y1 = diam/2*sin(arg1)
        coord1 = (i+1.5)*100
        for n in range(cols):
            arg2 = (n+1)*angle-PI
            x2 = diam/2*cos(arg2-(PI*1/2*PI))
            y2 = diam/2*sin(arg2-(PI*1/2*PI))
            coord2 = (n+1.5)*100
            createPoint(coord1, diam-35, x1, y1)
            createPoint(diam-35, coord2, x2, y2)
            x = coord1 + x1
            y = coord2 + y2
            grPoi(x,y)
            
            createCurve(x,y,i,n)
          
         
def createCurve(x,y,i,n):
    global shapes
    if angle<= 2*PI+0.05:
        shapes[i][n].append(x)
        shapes[i][n].append(y)
    noFill()
    strokeWeight(1)
    stroke(255,0,255)
    beginShape()
    for p in range(0, len(shapes[i][n]), 2):
        vertex(shapes[i][n][p], shapes[i][n][p+1])
    endShape()

def pow(a,b):
    out = a
    for i in range(b-1):
        out = out*a
    return out

def circles():
    global rad
    strokeWeight(1)
    for i in range(width/scale-1):
        noFill()
        stroke(255)
        coord = (i+1.5)*100
        ellipse(coord, diam-35, diam, diam)
        ellipse(diam-35, coord, diam, diam)

def grPoi(x,y):
    stroke(0,255,0)
    strokeWeight(8)
    point(x, y)
    noFill()

def createPoint(x,y,x1,y1):
    stroke(255,0,0)
    strokeWeight(7)
    point(x+x1, y+y1)
    strokeWeight(1)
    stroke(255,20)
    if y is diam-35:
        line(x+x1, y+y1, x+x1, height)
    elif x is diam-35:
        line(x+x1, y+y1, width, y+y1)

def setArray():
    global shapes
    for i in range(rowss):
        temp1 = []
        for n in range(cols):
            temp2 = []
            temp1.append(temp2)
        shapes.append(temp1)
