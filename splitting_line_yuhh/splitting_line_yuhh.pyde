sh = []
i = 1
dirvecs = []


def setup():
    global sh
    size(500,500)
    background(0)
    stroke(255)
    reset()
    
def draw():
    background(0)
    global sh, i, dirvecs
    lastvec = PVector(sh[len(sh)-1].x, sh[len(sh)-1].y).add(PVector(dirvecs[len(dirvecs)-1].x, dirvecs[len(dirvecs)-1].y).mult(i))
    if sh[len(sh)-1].x <= width-20 and sh[len(sh)-1].y >= 20:
        if len(sh) <2:
            sh.append(lastvec)
        else:
            sh[len(sh)-1] = lastvec
        i += 2
    sFromSh()
    
    
def mousePressed():
    reset()
    redraw()
    
def keyPressed():
    if key == "n" or key == " " or key == "1":
        #newlines()
        randang()
    
def sFromSh():
    strokeWeight(4)
    stroke(255)
    beginShape()
    for i in sh:
        vertex(i.x, i.y)
    endShape()    
    
    
def randang():
    global dirvecs
    ang = radians(round(random(0, 180)))
    print(ang)
    x = cos(ang) * 1
    y = cos(ang) * 1
    ol = PVector(dirvecs[len(dirvecs)-1].x, dirvecs[len(dirvecs)-1].y)
    dirvecs.append(PVector(x,y))
    
    
def reset():
    global sh, dirvecs, i
    sh = []
    sh.append(PVector(20, height/2))
    dirvecs = []
    dirvecs.append(PVector(1,0))
    dirvecs.append(PVector(1,0))
    i = 0
