points = []
i = 0
sPoints = []
import random

def setup():
    global i
    global sPoints
    for i in range(0,6):
        points.append(None)
        randPoint(i)
    for i in range(0,2):
        sPoints.append(None)    
    getSPoints(2)
    drawSPoints()
    size(500,500)
    background(0)
    conn()
    i = i+1


def draw():
    drawSPoints()
    global i
    if i % 70 is 0:
        background(0)
        randpo()
        conn()
        getSPoints(2)
    i = i+1
    drawSPoints()
    #noLoop()

def randPoint(num):
    x = random.random()*width
    y = random.random()*height
    stroke(0)
    fill(255)
    ellipse(x,y,20,20)
    a = PVector(x,y)
    points[num] = a

def randpo():
    global points
    global sPoints
    for i in range(0,5):
        if i not in sPoints:
            print(i)
            print(sPoints)
            randPoint(i)
    noStroke()
    fill(255,0,255)
    
def drawSPoints():
    for i in sPoints:
        ellipse(points[i].x, points[i].y, 30, 30)
    noStroke()
    fill(255,0,255)
    
def getSPoints(l):
    global sPoints
    for i in range(0,l):
        ran = random.randint(0,5)
        while ran in sPoints:
            ran = random.randint(0,5)
        sPoints[i] = ran

def conn():
    global points
    for i in range(len(points)):
        for n in range(len(points)):
            if i is not n:
                stroke(255)
                line(points[n].x,points[n].y,points[i].x,points[i].y)
