add_library('minim')
rad = 100
intensity = 5
time = 0

def setup():
    minim = Minim(this)
    global song
    song = minim.loadFile ("short.mp3", 500)
    song.play()
    size (500,500)
    frameRate (60) #Wenn zu hoch -> viel zu flashy und seizure-inducing
    
def draw():
    background(0)
    stroke (255)
    for i in range((song.bufferSize() - 1)):
        None
       
    #Hintergrundfarbe in Abhängigkeit des Audios     
    #r = noise(int(100*song.left.get(0))*0.01)
    #g = noise(int(100*song.right.get(1))*0.01)
    #b = noise(r+g)
    #background(r*255, g*255, b*255)
    
    #Informationen zum Output drucken
    text(song.left.get(i)*50, 50, 50)
    text(song.left.get(i)*50-song.right.get(i)*50, 100, 50) #Differenz links u. rechs
    text(song.left.get(0)*50-song.right.get(250)*50, 150, 50)
    text(frameRate, 1, 15)
    
    #Welle malen
    noFill()
    stroke(255)
    beginShape()
    for b in range(int(TWO_PI*1000)):
        i = b/1000
        x = cos(i)
        y = sin(i)
        off = song.left.get(i)
        xoff = x*rad*off
        yoff = y*rad
        vertex(width/2+xoff, height/2+yoff)
        #line (i, 100+song.left.get(i)*50, i+1, 100+song.left.get(i+1)*50)
        
    endShape(CLOSE)
