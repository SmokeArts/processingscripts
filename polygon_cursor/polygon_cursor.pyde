mx = 0
my = 0
off = 75
pts = []

def setup():
    fullScreen()
    #size(500,500)
    background(0)
    
def draw():
    background(0)
    global my, mx, off, pts
    mx = mouseX
    my = mouseY
    fill(255,100,255)
    noStroke()
    ellipse(mx, my, 30, 30)
    if len(pts) is 0:
        for i in range(0,10):
            xof = mx + int(round(random(-off,off)))
            yof = my + int(round(random(-off,off)))
            pvec = PVector(xof, yof)
            pts.append(pvec)
    

    pts.pop(0)
    newp = PVector(mx + int(round(random(-off,off))),
                    my + int(round(random(-off,off))))
    pts.append(newp)
    
    fill(200)
    
    for i in range(0, len(pts)):
        fill(255)
        noStroke()
        ellipse(pts[i].x, pts[i].y, 10, 10)
        for n in range(0,2):
            ranp = int(random(round(0, len(pts))))
            strokeWeight(1)
            stroke(255,0,255)
            noFill()
            line(pts[i].x, pts[i].y, pts[n].x, pts[n].y)
